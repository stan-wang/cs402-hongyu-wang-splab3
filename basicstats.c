//Hongyu Wang    November 19th，2020
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

FILE *open_file(char *fileName){
    if(fopen(fileName, "r") == NULL){
        printf("Error read file");
        exit(0);
    }
}

double get_mean(float *p, int n){
    double sum = 0;
    for(int i=0; i<n; i++){
        sum = sum+p[i];
    }
    return sum/n;
}

double get_stddev(float *p, int n){
    double stddev = 0;
    double mean = get_mean(p, n);
    for(int i=0; i<n; i++){
        stddev += pow(p[i] - mean, 2);
    }
    return sqrt(stddev/n);
}

double get_median(float *cur_arr, int n){
    if(n%2 == 1){
        return(cur_arr[n/2]);
    }else{
        return ((cur_arr[n/2-1] + cur_arr[n/2])/2);
    }
}

void swap(float *var1, float *var2){
    float temp = *var1;
    *var1 = *var2;
    *var2 = temp;
}

float sort(float *cur_arr, int n){
    int i, j, min_idx;
    for(i=0; i<n-1; i++){
        min_idx = i;
        for(j=i+1; j<n; j++){
            if(cur_arr[j]<cur_arr[min_idx]){
                min_idx = j;
            }
        }
        swap(&cur_arr[min_idx], &cur_arr[i]);
    }
}

int main(int argc, char *argv[]){
    if(argc < 2){
        printf("Pass filename to read from...\n");
        return 0;
    }
    int n = 20, length_arr = 0;
    float *cur_arr = (float *) malloc(n * sizeof(float));
    char *fname = argv[1];
    FILE *fp = open_file(fname);
    while(!feof(fp)){
        fscanf(fp, "%f\n", (cur_arr+length_arr));
        length_arr++;
        if(length_arr == n){
            float *new_arr = (float *)malloc(n*2*sizeof(float));
            memcpy(new_arr, cur_arr, length_arr*sizeof(float));
            free(cur_arr);
            cur_arr = new_arr;
            n = n*2;
        }
    }
    fclose(fp);
    double mean = get_mean(cur_arr, length_arr);
    double stddev = get_stddev(cur_arr, length_arr);
    sort(cur_arr, length_arr);
    double median = get_median(cur_arr, length_arr);
    printf("Results:\n----------\n");
    printf("%15s %14d\n", "Num values: ", length_arr);
    printf("%15s %14.3f\n", "mean: ",mean);
    printf("%15s %14.3f\n", "median: ", median);
    printf("%15s %14.3f\n", "stddev: ", stddev);
    printf("%25s %2d\n", "Unused array capacity:",n-length_arr);

}